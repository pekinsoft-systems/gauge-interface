# Gauge Interface

This project is related to our [NTOS Project](https://gitlab/pekinsoft-systems/ntos) for creating a dashboard-like interface that truck drivers are already used to interacting with. Whereas we had initially been looking at possible docking frameworks and the [JFreeChart](https://www.jfree.org/jfreechart/) open source chart library, we decided that type of interface was very "yesteryear", and we wanted to do something completely different.

Therefore, after the team talked about it, and one of them read the book *[Filthy Rich Clients](https://www.amazon.com/Filthy-Rich-Clients-Developing-Applications/dp/0132413930)* by Chet Haase and Romain Guy, we discovered a much better interface strategy. Since PekinSOFT Systems already tends toward using old, abandoned APIs, we saw absolutely nothing wrong with rethinking our interface to use the old JSR-296 Swing Application Framework, as well as the Timing Framework, etc. Therefore, we started this project.

This project is basically here for us to sandbox drawing gauges (like speedometers/tachometers, fuel gauges, oil gauges, etc.) that will act as the charts we were going to use *JFreeChart* for displaying current financial states of the company. However, by doing so in this manner, these "gauges" that show the company's financial health, miles/kilometers per gallon/10-kilometers, will be the primary interface for the application. These gauges will have clickable areas in them (like hotspots) that will allow the user to enter information into the database. The initial dashboard design during our discussions looked like this:

![Rendered Dashboard Layout](./.doc-images/NTOS-with-RENDERED-Dashboar-Layout.png)

This project, which contains only a single class of any importance, named `Gauge` which creates a gauge like a speedometer, though without any data being supplied to it other than the current value:

![Speedometer Gauge](./.doc-images/Rendered-Speedometer-Gauge.png)

## Contributing
As with all PekinSOFT Systems projects, we welcome anyone that would like to contribute to this project. Whether it be helping with the development, writing documentation, or translating resources and documentation, we greatly appreciate all help you are willing to offer.

## Authors and acknowledgment

| Title | Name | Location |
|-----------|--- |--- |
| Owner / Lead Developer | Sean Carrick | Illinois, USA  |
| Lead Tester / Developer | Jiří Kovalský | Bohimin, Czech Republic |
| Contributer / Developer | Kevin Nathan | Arizona, USA |

## License
This project is released under the terms of [The GNU Public License](https://www.gnu.org/licenses/gpl-3.0.en.html), or any later version.

## Project status
The *Gauge Interface Project* is in its start-up phase, though will be actively developed over the coming weeks/months to bring it to a mature enough point where we can start using it in the *NTOS Project*.
